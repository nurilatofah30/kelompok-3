@extends('template.main')
@section('judul','Data Peminjam Buku')
@section('peminjam','active')
@section('konten')
<div class="product-card shadow">

    <form action="/peminjam" method="post">
        @csrf
        <div class="mb-3">
            <label class="title-section-content" for="">Nama</label>
            <input value="{{old('nama')}}" name="nama" type="text"
                class="form-control @error('nama') is-invalid @enderror" placeholder="Masukkan Nama Lengkap Anda">
            @error('nama')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">No Telepon</label>
            <input value="{{old('no_telp')}}" name="no_telp" type="text"
                class="form-control @error('no_telp') is-invalid @enderror" placeholder="Masukkan No Telepon Lengkap Anda">
            @error('no_telp')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        
        <div class="mb-3">
            <label class="title-section-content" for="">Nama Buku</label>
            <input value="{{old('nama_buku')}}" name="nama_buku" type="text"
                class="form-control @error('nama_buku') is-invalid @enderror" placeholder="Masukkan Nama Buku Lengkap Anda">
            @error('nama_buku')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Tanggal Pinjam</label>
            <input value="{{old('tgl_pinjam')}}" name="tgl_pinjam" type="text"
                class="form-control @error('tgl_pinjam') is-invalid @enderror" placeholder="Masukkan Tanggal Pinjam Lengkap Anda">
            @error('tgl_pinjam')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Tanggal Pengembalian</label>
            <input value="{{old('tgl_pengembalian')}}" name="tgl_pengembalian" type="date"
                class="form-control @error('tgl_pengembalian') is-invalid @enderror" placeholder="Masukkan Tanggal Pengembalian Lengkap Anda">
            @error('tgl_pengembalian')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>



        <div class="mb-3 ">
            <a href="/peminjam" class="btn btn-secondary">Kembali</a>
            <button class="btn btn-primary" type="submit">Tambah Data</button>
        </div>



    </form>

</div>


@endsection
