@extends('template.main')
@section('judul','Detail Peminjaman Buku')
@section('peminjam','active')
@section('konten')
<ul class="list-group">
    <li class="list-group-item">Nama : {{$peminjam->nama}}</li>
    <li class="list-group-item">No Telepon : {{$peminjam->no_telp}}</li>
    <li class="list-group-item">Nama Buku : {{$peminjam->nama_buku}}</li>
    <li class="list-group-item">Tanggal Pinjam : {{$peminjam->tgl_pinjam}}</li>
    <li class="list-group-item">Tanggal Pengembalian : {{$peminjam->tgl_pengembalian}}</li>
    <li class="list-group-item">
        <a href="/peminjam" class="btn btn-secondary">Kembali</a>
    </li>
</ul>
@endsection
