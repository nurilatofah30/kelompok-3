<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Anggota </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>

<body>
    <div class="">
        <div class="row">
            <div class="col-12 pt-5">
                <h3 class="text-center font-weight-bold" style="font-size: 30px;">
                    Laporan Data Anggota 
                </h3>
                <table class="table table-striped">
                    <thead class="bg-secondary">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">nama</th>
                            <th scope="col">No Telepon</th>
                            <th scope="col">Jenis Kelamin</th>
                            <th scope="col">Alamat</th>
                            <th scope="col">Tempat Lahir</th>
                            <th scope="col">Tanggal Lahir</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($anggota as $x )
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$x->nama}}</td>
                            <td>{{$x->no_telepon}}</td>
                            <td>{{$x->jenis_kelamin	}}</td>
                            <td>{{$x->alamat}}</td>
                            <td>{{$x->tempat_lahir}}</td>
                            <td>{{$x->tanggal_lahir}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class=" float-right text-right">
                    <p>
                        Pekanbaru, {{ date('d F Y') }}
                        <br>
                        <br>
                        <br>
                    </p>
                    <p>
                        <ins>
                            Hananiah, Febry, Thariq
                            
                        </ins>
                    </p>
                    <p>
                        Kelompok 3
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>

</html>
