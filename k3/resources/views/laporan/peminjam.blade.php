<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Peminjaman Buku </title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>

<body>
    <div class="">
        <div class="row">
            <div class="col-12 pt-5">
                <h3 class="text-center font-weight-bold" style="font-size: 30px;">
                    Laporan Data Peminjaman Buku 
                </h3>
                <table class="table table-striped">
                    <thead class="bg-secondary">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">No Telp</th>
                            <th scope="col">Nama Buku</th>
                            <th scope="col">Tanggal Pinjam</th>
                            <th scope="col">Tanggal Pengembalian</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($peminjam as $x )
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$x->nama}}</td>
                            <td>{{$x->no_telp}}</td>
                            <td>{{$x->nama_buku	}}</td>
                            <td>{{$x->tgl_pinjam}}</td>
                            <td>{{$x->tgl_pengembalian}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
                <div class=" float-right text-right">
                    <p>
                        Pekanbaru, {{ date('d F Y') }}
                        <br>
                        <br>
                        <br>
                    </p>
                    <p>
                        <ins>
                            Hananiah, Febry, Thariq
                            
                        </ins>
                    </p>
                    <p>
                        Kelompok 3
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
</body>

</html>
