<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Data Pengguna</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/css/bootstrap.min.css" integrity="sha384-xOolHFLEh07PJGoPkLv1IbcEPTNtaed2xpHsD9ESMhqIYd0nLMwNLD69Npy4HI+N" crossorigin="anonymous">
</head>
<body>
    <div class="justify-content-center">
        <div class="row">
            <div class="col-12 pt-5">
                <p class="text-center font-weight-bold" style="font-size: 30px;">Laporan Data Pengguna</p>
                <table class="table table-striped">
                    <thead class="bg-secondary">
                        <tr>
                            <th scope="col">No</th>
                            <th scope="col">Nama</th>
                            <th scope="col">Email</th>
                            <th scope="col">Phone</th>
                            <th scope="col">Role</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($user as $x )
                        <tr>
                            <th scope="row">{{$loop->iteration}}</th>
                            <td>{{$x->name}}</td>
                            <td>{{$x->email}}</td>
                            <td>{{$x->phone}}</td>
                            <td>{{$x->role}}</td>
                        </tr>
                        ntar
                        @endforeach
                    </tbody>
                </table>
                <div class=" float-right text-right">
                    <p>
                        Pekanbaru, {{ date('d F Y') }}
                        <br>
                        <br>
                        <br>
                    </p>
                    <p>
                        <ins>
                            Hananiah, Febry, Thariq
                        </ins>
                    </p>
                    <p>
                        Kelompok 3
                    </p>
                </div>
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
    
</body>

</html>
