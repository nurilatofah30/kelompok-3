@extends('template.main')
@section('judul','Detail Anggota')
@section('anggota','active')
@section('konten')
<ul class="list-group">
    <li class="list-group-item">Nama : {{$anggota->nama}}</li>
    <li class="list-group-item">Kategori : {{$anggota->kategori}}</li>
    <li class="list-group-item">Pengarang : {{$anggota->pengarang}}</li>
    <li class="list-group-item">Alamat : {{$anggota->alamat}}</li>
    <li class="list-group-item">Tempat Lahir : {{$anggota->tempat_lahir}}</li>
    <li class="list-group-item">Tanggal Lahir : {{Carbon\Carbon::create($anggota->tanggal_lahir)->translatedFormat('l, d F Y')}}</li>
    <li class="list-group-item">
        <a href="/anggota" class="btn btn-secondary">Kembali</a>
    </li>
</ul>
@endsection
