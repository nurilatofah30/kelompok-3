@extends('template.main')
@section('judul','Data Anggota')
@section('anggota','active')
@section('konten')
<div class="product-card shadow">

    <form action="/anggota/{{$anggota->id}}" method="post">
        @method('put')
        @csrf
        <div class="mb-3">
            <label class="title-section-content" for="">Nama</label>
            <input value="{{$anggota->nama}}" name="nama" type="text"
                class="form-control @error('nama') is-invalid @enderror" >
            @error('nama')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">No Telepon</label>
            <input value="{{$anggota->no_telepon}}" name="no_telepon" type="text"
                class="form-control @error('no_telepon') is-invalid @enderror">
            @error('no_telepon')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Jenis Kelamin</label>
            <select name="jenis_kelamin" id="" class="form-control @error('jenis_kelamin') is-invalid @enderror">
            <option value="">--Pilih Jenis Kelamin--</option>
            <option value="perempuan">Perempuan</option>
            <option value="laki">Laki-laki</option>
        </select>
            @error('jenis_kelamin')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Alamat</label>
            <input value="{{$anggota->alamat}}" name="alamat" type="text"
                class="form-control @error('alamat') is-invalid @enderror">
            @error('alamat')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Tempat Lahir</label>
            <input value="{{$anggota->tempat_lahir}}" name="tempat_lahir" type="text"
                class="form-control @error('tempat_lahir') is-invalid @enderror">
            @error('tempat_lahir')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3">
            <label class="title-section-content" for="">Tanggal Lahir</label>
            <input value="{{$anggota->tanggal_lahir}}" name="tanggal_lahir" type="date"
                class="form-control @error('tanggal_lahir') is-invalid @enderror">
            @error('tanggal_lahir')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>



        <div class="mb-3 ">
            <a href="/anggota" class="btn btn-secondary">Kembali</a>
            <button class="btn btn-primary" type="submit">Edit Data</button>
        </div>



    </form>

</div>


@endsection
