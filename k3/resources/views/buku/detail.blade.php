@extends('template.main')
@section('judul','Detail Buku')
@section('buku','active')
@section('konten')
<ul class="list-group">
    <li class="list-group-item">Judul : {{$buku->judul}}</li>
    <li class="list-group-item">Kategori : {{$buku->kategori}}</li>
    <li class="list-group-item">Pengarang : {{$buku->pengarang}}</li>
    <li class="list-group-item">Kode Buku : {{$buku->kode_buku}}</li>
    <li class="list-group-item">Jumlah Halaman : {{$buku->jumlah_halaman}}</li>
    <li class="list-group-item">Tahun Terbit : {{$buku->tahun_terbit}}</li>
    <li class="list-group-item">Penerbit : {{$buku->penerbit}}</li>
    <li class="list-group-item">
        <a href="/buku" class="btn btn-secondary">Kembali</a>
    </li>
</ul>
@endsection
