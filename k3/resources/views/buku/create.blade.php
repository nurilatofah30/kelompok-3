@extends('template.main')
@section('judul','Data Buku')
@section('buku','active')
@section('konten')
<div class="product-card shadow">

<form action="/buku" method="post">
    @csrf
    <div class="mb-3">
            <label class="title-section-content" for="">Judul</label>
            <input value="{{old('judul')}}" name="judul" type="text"
                class="form-control @error('judul') is-invalid @enderror" placeholder="Masukkan judul Lengkap Anda">
            @error('judul')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Kategori</label>
            <input value="{{old('kategori')}}" name="kategori" type="text"
                class="form-control @error('kategori') is-invalid @enderror" placeholder="Masukkan kategori Lengkap Anda">
            @error('kategori')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Pengarang</label>
            <input value="{{old('pengarang')}}" name="pengarang" type="text"
                class="form-control @error('pengarang') is-invalid @enderror" placeholder="Masukkan pengarang Lengkap Anda">
            @error('pengarang')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Kode Buku</label>
            <input value="{{old('kode_buku')}}" name="kode_buku" type="text"
                class="form-control @error('kode_buku') is-invalid @enderror" placeholder="Masukkan kode_buku Lengkap Anda">
            @error('kode_buku')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Jumlah Halaman</label>
            <input value="{{old('jumlah_halaman')}}" name="jumlah_halaman" type="text"
                class="form-control @error('jumlah_halaman') is-invalid @enderror" placeholder="Masukkan jumlah_halaman Lengkap Anda">
            @error('jumlah_halaman')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">jumlah_buku</label>
            <input value="{{old('jumlah_buku')}}" name="jumlah_buku" type="text"
                class="form-control @error('jumlah_buku') is-invalid @enderror" placeholder="Masukkan jumlah_buku Lengkap Anda">
            @error('jumlah_buku')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">tahun_terbit</label>
            <input value="{{old('tahun_terbit')}}" name="tahun_terbit" type="text"
                class="form-control @error('tahun_terbit') is-invalid @enderror" placeholder="Masukkan tahun_terbit Lengkap Anda">
            @error('tahun_terbit')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">penerbit</label>
            <input value="{{old('penerbit')}}" name="penerbit" type="text"
                class="form-control @error('penerbit') is-invalid @enderror" placeholder="Masukkan penerbit Lengkap Anda">
            @error('penerbit')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3 ">
        <a href="/buku" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Tambah Data</button>
    </div>
    </div>


</form>

</div>


@endsection