@extends('template.main')
@section('judul','Data Buku')
@section('buku','active')
@section('konten')
<div class="product-card shadow">

<form action="/buku/{{$buku->id}}" method="POST"> 
    @method('put')
    @csrf

    <div class="mb-3">
            <label class="title-section-content" for="">Judul</label>
            <input value="{{$buku->judul}}" name="judul" type="text"
                class="form-control @error('judul') is-invalid @enderror" >
            @error('judul')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Kategori</label>
            <input value="{{$buku->kategori}}" name="kategori" type="text"
                class="form-control @error('kategori') is-invalid @enderror" >
            @error('kategori')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Pengarang</label>
            <input value="{{$buku->pengarang}}" name="pengarang" type="text"
                class="form-control @error('pengarang') is-invalid @enderror" >
            @error('pengarang')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Kode Buku</label>
            <input value="{{$buku->kode_buku}}" name="kode_buku" type="text"
                class="form-control @error('kode_buku') is-invalid @enderror" >
            @error('kode_buku')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    
    <div class="mb-3">
            <label class="title-section-content" for="">Jumlah Halaman</label>
            <input value="{{$buku->jumlah_halaman}}" name="jumlah_halaman" type="text"
                class="form-control @error('jumlah_halaman') is-invalid @enderror" >
            @error('jumlah_halaman')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">jumlah_buku</label>
            <input value="{{$buku->jumlah_buku}}" name="jumlah_buku" type="text"
                class="form-control @error('jumlah_buku') is-invalid @enderror"  >
            @error('jumlah_buku')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">tahun_terbit</label>
            <input value="{{$buku->tahun_terbit}}" name="tahun_terbit" type="text"
                class="form-control @error('tahun_terbit') is-invalid @enderror" >
            @error('tahun_terbit')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
    <div class="mb-3">
            <label class="title-section-content" for="">penerbit</label>
            <input value="{{$buku->penerbit}}" name="penerbit" type="text"
                class="form-control @error('penerbit') is-invalid @enderror" >
            @error('penerbit')
            <div class="invalid-feedback"> {{$message}} </div>
            @enderror
        </div>
        <div class="mb-3 ">
        <a href="/buku" class="btn btn-secondary">Kembali</a>
        <button class="btn btn-primary" type="submit">Edit Data</button>
    </div>
    </div>


</form>

</div>


@endsection