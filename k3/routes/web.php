<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\BukuController;
use App\Http\Controllers\AnggotaController;
use App\Http\Controllers\PeminjamController;
use App\Http\Controllers\LaporanController;




/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::get('/', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/', [LoginController::class, 'login'])->middleware('guest');
Route::get('/logout', [LoginController::class, 'logout'])->middleware('auth');
Route::get('/home', [LoginController::class, 'dashboard'])->middleware('auth');

Route::resource('/user', UserController::class)->middleware('auth');
Route::resource('/buku', BukuController::class)->middleware('auth');
Route::resource('/anggota', AnggotaController::class)->middleware('auth');
Route::resource('/peminjam', PeminjamController::class)->middleware('auth');

Route::get('/reportbuku', [LaporanController::class, 'buku'])->middleware('auth');
Route::get('/reportanggota', [LaporanController::class, 'anggota'])->middleware('auth');
Route::get('/reportpeminjam', [LaporanController::class, 'peminjam'])->middleware('auth');
Route::get('/reportuser', [LaporanController::class, 'user'])->middleware('auth');

