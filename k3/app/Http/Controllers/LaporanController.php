<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Anggota;
use App\Models\Peminjam;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function buku()
    {
        $buku=Buku::all();
        $pdf=Pdf::loadView('laporan.buku', compact('buku'))->setPaper('a4', 'landscape');
        return $pdf->stream('databuku.pdf');
    }
    public function anggota()
    {
        $anggota=Anggota::all();
        $pdf=Pdf::loadView('laporan.anggota', compact('anggota'))->setPaper('a4', 'landscape');
        return $pdf->stream('dataanggota.pdf');
    }
    public function peminjam()
    {
        $peminjam=Peminjam::all();
        $pdf=Pdf::loadView('laporan.peminjam', compact('peminjam'))->setPaper('a4', 'landscape');
        return $pdf->stream('datapeminjam.pdf');
    }
    public function user()
    {
        $user=User::all();
        $pdf=Pdf::loadView('laporan.user', compact('user'))->setPaper('a4', 'landscape');
        return $pdf->stream('datauser.pdf');
    }
}
