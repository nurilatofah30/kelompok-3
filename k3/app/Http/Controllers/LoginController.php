<?php

namespace App\Http\Controllers;

use App\Models\Buku;
use App\Models\Peminjam;
use App\Models\Anggota;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index(){
        return view('login');
    }
    public function login(Request $request){
        $request->validate([
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $credentials=$request->only('email','password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect('/user');
        }

        return back()->withErrors([
            'error' => 'Email/Password Tidak Terdaftar.',
        ])->withInput();
    }
    public function logout(Request $request){
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
    public function dashboard()
    { 
        $buku=Buku::count();
        $peminjam=Peminjam::count();
        $anggota=Anggota::count();
        $user=User::count();
        return view('dashboard.index', compact('buku','peminjam', 'anggota', 'user'));
    }
}

