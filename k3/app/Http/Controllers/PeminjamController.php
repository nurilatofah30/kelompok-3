<?php

namespace App\Http\Controllers;

use App\Models\Peminjam;
use Illuminate\Http\Request;

class PeminjamController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data=Peminjam::all();
        return view('peminjam.index', compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('peminjam.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'no_telp' => 'required',
            'nama_buku' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_pengembalian' => 'required',
        ]);
        
        $data=$request->all();
        Peminjam::create($data);
        return redirect('/peminjam')->with('sukses','Data Berhasil Ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function show(Peminjam $peminjam)
    {
        return view('peminjam.detail', compact('peminjam'));
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function edit(Peminjam $peminjam)
    {
        return view('peminjam.edit', compact('peminjam'));
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Peminjam $peminjam)
    {
        $request->validate([
            'nama' => 'required',
            'no_telp' => 'required',
            'nama_buku' => 'required',
            'tgl_pinjam' => 'required',
            'tgl_pengembalian' => 'required',
        ]);
        $db= $request->all();
        $dl= Peminjam::findOrfail($peminjam->id);
        $dl->update($db);
        return redirect('/peminjam')->with('sukses','Data Berhasil Diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Peminjam  $peminjam
     * @return \Illuminate\Http\Response
     */
    public function destroy(Peminjam $peminjam)
    {
        Peminjam::destroy($peminjam->id);
        return redirect('/peminjam')->with('sukses','Data Berhasil Dihapus');
    }
}
